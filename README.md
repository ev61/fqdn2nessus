# FQDN2Nessus

This is a simple shellscript which takes an FQDN (eg www.google.com) and produces a target list suitable for pasting into nessus.

## Purpose

Hostnames can (and often do) resolve to multiple addresses, for instance:

````
$ host www.yahoo.com
www.yahoo.com is an alias for new-fp-shed.wg1.b.yahoo.com.
new-fp-shed.wg1.b.yahoo.com has address 87.248.100.216
new-fp-shed.wg1.b.yahoo.com has address 87.248.100.215
new-fp-shed.wg1.b.yahoo.com has IPv6 address 2a00:1288:110:c305::1:8001
new-fp-shed.wg1.b.yahoo.com has IPv6 address 2a00:1288:110:c305::1:8000
````

The well known website "www.yahoo.com" resolves to 4 addresses, 2 of which are IPv6 and the other 2 are legacy IPv4. 
If you browse to this site, your browser will pick one of these addresses at random to connect to. If it's unable to 
do so, it may retry using a different address. This works fine for end user browsers, as the user doesn't typically
care which server they load the site from.

Nessus acts the same way, if you specify "www.yahoo.com" as a target, Nessus will do a DNS lookup, see 4 addresses
and then pick one of them at random - ignoring the rest. While this is reasonable behaviour for a browser, it is 
quite broken behaviour for a scanner.
Nessus documentation says this about hostnames:

> The single host is scanned. If the hostname resolves to multiple addresses the address to scan is the first IPv4 address or if it did not resolve to an IPv4 address, the first IPv6 address.

If a host resolves to multiple addresses and you want to do a thorough job of scanning, you need to scan ALL OF THE
ADDRESSES. In many cases all of the addresses will be configured exactly the same, but there are always anomalous 
cases - and it is exactly these anomalous cases that are the purpose for scanning.

Thus the tool fqdn2nessus allows you to perform the DNS resolution, and then pin hostnames and IP addresses together.

It is possible to manually resolve the IP addresses and specify these as targets. Doing so is not recommended, as
Nessus will no longer know the hostname associated with each IP. Many protocols (especially HTTP and SSL) can 
return different content depending on the hostname used. Scanning such targets without knowing the correct hostname 
can often result in content being missed. For a more detailed explanation, see:
[https://www.trustwave.com/en-us/resources/blogs/spiderlabs-blog/still-scanning-ip-addresses-you-re-doing-it-wrong/](https://www.trustwave.com/en-us/resources/blogs/spiderlabs-blog/still-scanning-ip-addresses-you-re-doing-it-wrong/)

## Usage

`fqdn2nessus HOSTNAME`

For example:

````
$ fqdn2nessus www.yahoo.com
www.yahoo.com[2406:2000:e4:1605::9000]
www.yahoo.com[2406:2000:e4:1605::9001]
www.yahoo.com[202.165.107.50]
www.yahoo.com[202.165.107.49]
````

